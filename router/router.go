package router

import (
	"bitbucket.org/hermes_canuto/go-rest-mysql/controller"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var App *echo.Echo

func init() {
	App = echo.New()

	App.GET("/", controller.Home)


	u := App.Group("/api_usuario")
	u.Use(middleware.JWT([]byte("Vamos todo nessa amigos")))
	u.GET("/usuario/:id", controller.GetUser)
	u.GET("/usuario", controller.GetAllUser)


	login := App.Group("/api_login")
	login.POST("/login", controller.Login)
	login.GET("/go", controller.Go)


}
