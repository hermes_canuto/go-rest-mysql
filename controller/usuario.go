package controller

import (
	"bitbucket.org/hermes_canuto/go-rest-mysql/model"

	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

func GetAllUser(c echo.Context) error {
	var usuariosDB model.UsuariosDB
	var u model.Usuarios
	if err := model.UsuarioModel.Find().All(&usuariosDB) ; err != nil {
		return c.JSON(http.StatusBadRequest,
			map[string]string{
				"mensagem": "Erro ao tentar recuperar os dados!",
				"erro":     err.Error(),
			})
	}
	usuariosDB.ScanAll(&u)
	return c.JSON(http.StatusOK, u)
}

func GetUser(c echo.Context) error {
	var usuario model.UsuarioDB
	var u model.Usuario
	usuarioID, _ := strconv.Atoi(c.Param("id"))
	model.UsuarioModel.Find("id_usuario=?", usuarioID).One(&usuario)
	usuario.ScanOne(&u)
	if u.Id_usuario > 0 {
		return c.JSON(http.StatusOK, u)
	} else {
		return c.JSON(http.StatusNotFound,
			map[string]string{"mensagem": "Registro não encontrado"})
	}

}
