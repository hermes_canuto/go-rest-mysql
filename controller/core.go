package controller

import (
	"fmt"
	"github.com/labstack/echo"
	"net/http"
)

func Home(c echo.Context) error {
	r := c.Request()
	baseURL := c.Scheme() + "://" + r.Host
	currentURL := c.Scheme() + "://" + r.Host + r.URL.Path
	data := map[string]interface{}{
		"titulo" : "HOME",
		"url" : baseURL,
		"current" : currentURL,
	}
	return c.Render(http.StatusOK,"home.html",data)
}


func Go(c echo.Context) error {

	m := echo.Map{}
	fmt.Println("teste", m)

	return c.JSON(200, "Hello" )
}