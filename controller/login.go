package controller

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/hermes_canuto/go-rest-mysql/lib"
	"bitbucket.org/hermes_canuto/go-rest-mysql/model"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

func Login(c echo.Context) error {

	p := new(model.UsuarioLogin)
	if err := c.Bind(p); err == nil {
		if p.Ch_senha != "" && p.Ch_email != "" {
			var usuario model.UsuarioDB
			var u model.Usuario
			model.UsuarioModel.Find("ch_email", p.Ch_email).And("ch_senha", lib.GetMD5Hash(p.Ch_senha)).One(&usuario)
			fmt.Println(usuario)
			if usuario.Ch_token != "" {
				usuario.ScanOne(&u)
				return c.JSON(http.StatusOK, map[string]string{
					"token": GetJwtToken(u),
				})
			}
			return c.JSON(http.StatusBadRequest,
				map[string]string{"mensagem": "Registro não encontrado"})
		}
	}
	return c.JSON(http.StatusBadRequest,
		map[string]string{"mensagem": "Os campos são obrigatório!"})
}

func GetJwtToken(usuario model.Usuario) string {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = usuario.Ch_nome
	claims["token"] = usuario.Ch_token
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	t, err := token.SignedString([]byte("Vamos todo nessa amigos"))
	if err != nil {
		return "nao rolou"
	}
	return t
}