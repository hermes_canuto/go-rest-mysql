package main

import (
	"bitbucket.org/hermes_canuto/go-rest-mysql/router"
	_"bitbucket.org/hermes_canuto/go-rest-mysql/router"
	"github.com/MarcusMann/pongor-echo"
	"github.com/hermescanuto/goconfigfromjsonfile"
	"github.com/labstack/echo/middleware"
	"os"
)

func main() {
	env := os.Getenv("PORT")
	if env == "" {
		config := goconfigfromjsonfile.Getconfig()
		env = ":" + config.Server
	} else {
		env = ":" + env
	}
	//
	e := router.App

	p := pongor.GetRenderer(pongor.PongorOption{Reload: true})
	p.Directory = "view"
	e.Renderer = p

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"}}))

	e.Use(middleware.Logger())
	e.Logger.Fatal(e.Start(env))

}
