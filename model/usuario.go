package model

import (
	"bitbucket.org/hermes_canuto/go-rest-mysql/lib"
	"github.com/go-sql-driver/mysql"
)


//  representa a strutura para login
type UsuarioLogin struct {
	Ch_email string `db:"ch_email" json:"ch_email"`
	Ch_senha string `db:"ch_senha" json:"ch_senha"`
}

//  representa a tabela tb_usuario no banco de dados
type Usuario struct {
	Id_usuario      int    `db:"id_usuario" json:"id_usuario"`
	Id_perfil       int    `db:"id_perfil" json:"id_perfil"`
	Id_tipo_usuario int    `db:"id_tipo_usuario" json:"id_tipo_usuario"`
	Id_cargo        int    `db:"id_cargo" json:"id_cargo"`
	Id_status       int    `db:"id_status" json:"id_status"`
	Ch_nome         string `db:"ch_nome" json:"ch_nome"`
	Ch_sobrenome    string `db:"ch_sobrenome" json:"ch_sobrenome"`
	Ch_email        string `db:"ch_email" json:"ch_email"`
	Ch_senha        string `db:"ch_senha" json:"ch_senha"`
	Tp_camiseta     string `db:"tp_camiseta" json:"tp_camiseta"`
	Dt_nascimento   string `db:"dt_nascimento" json:"dt_nascimento"`
	Dt_inclusao     string `db:"dt_inclusao" json:"dt_inclusao"`
	Dt_atualizacao  string `db:"dt_atualizacao" json:"dt_atualizacao"`
	Dt_inativacao   string `db:"dt_inativacao" json:"dt_inativacao"`
	Ch_token        string `db:"ch_token" json:"ch_token"`
}

type Usuarios []Usuario

type UsuarioDB struct {
	Id_usuario      int            `db:"id_usuario" json:"id_usuario"`
	Id_perfil       int            `db:"id_perfil" json:"id_perfil"`
	Id_tipo_usuario int            `db:"id_tipo_usuario" json:"id_tipo_usuario"`
	Id_cargo        int            `db:"id_cargo" json:"id_cargo"`
	Id_status       int            `db:"id_status" json:"id_status"`
	Ch_nome         string         `db:"ch_nome" json:"ch_nome"`
	Ch_sobrenome    string         `db:"ch_sobrenome" json:"ch_sobrenome"`
	Ch_email        string         `db:"ch_email" json:"ch_email"`
	Ch_senha        string         `db:"ch_senha" json:"ch_senha"`
	Tp_camiseta     string         `db:"tp_camiseta" json:"tp_camiseta"`
	Dt_nascimento   mysql.NullTime `db:"dt_nascimento" json:"dt_nascimento"`
	Dt_inclusao     mysql.NullTime `db:"dt_inclusao" json:"dt_inclusao"`
	Dt_atualizacao  mysql.NullTime `db:"dt_atualizacao" json:"dt_atualizacao"`
	Dt_inativacao   mysql.NullTime `db:"dt_inativacao" json:"dt_inativacao"`
	Ch_token        string         `db:"ch_token" json:"ch_token"`
}

type UsuariosDB []UsuarioDB

// metodo para atribuir o valor que recebo do banco para um struc Usuario mais facil de ler
func (db *UsuarioDB) ScanOne(u *Usuario) {
	u.Id_usuario = db.Id_usuario
	u.Id_perfil = db.Id_perfil
	u.Id_tipo_usuario = db.Id_tipo_usuario
	u.Id_cargo = db.Id_cargo
	u.Id_status = db.Id_status
	u.Ch_nome = db.Ch_nome
	u.Ch_sobrenome = db.Ch_sobrenome
	u.Ch_email = db.Ch_email
	u.Ch_senha = db.Ch_senha
	u.Tp_camiseta = db.Tp_camiseta
	u.Dt_nascimento = db.Dt_nascimento.Time.String()[0:19]
	u.Dt_inclusao = db.Dt_inclusao.Time.String()[0:19]
	u.Dt_atualizacao = db.Dt_atualizacao.Time.String()[0:19]
	u.Dt_inativacao = db.Dt_atualizacao.Time.String()[0:19]
	u.Ch_token = db.Ch_token
}

func (db *UsuariosDB) ScanAll(u *Usuarios) {
	var r Usuario
	for _, item := range *db {
		item.ScanOne(&r)
		*u = append(*u, r)
	}
}

var UsuarioModel = lib.Sess.Collection("tb_usuario")
