package lib

import (
	"github.com/hermescanuto/goconfigfromjsonfile"
	"log"
	"upper.io/db.v3/mysql"
)

var config = goconfigfromjsonfile.Getconfig()

var comm = mysql.ConnectionURL{Host: config.Host,
	User:     config.User,
	Password: config.Password,
	Database: config.Database}

var Sess, Err = mysql.Open(comm)

func init() {
	if Err != nil {
		log.Fatal(Err.Error())
	}
	Sess.SetLogging(false)
}
